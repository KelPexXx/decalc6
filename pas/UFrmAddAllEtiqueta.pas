unit UFrmAddAllEtiqueta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, PngSpeedButton, ExtCtrls, JvExMask, JvSpin, DB, StdCtrls,
  Mask, DBCtrls, ComCtrls;

type
  TFrmAddAllEtiqueta = class(TForm)
    pTop: TPanel;
    pBottom: TPanel;
    btnAdicionar: TPngSpeedButton;
    btnCancelar: TPngSpeedButton;
    btnAnterior: TPngSpeedButton;
    btnProximo: TPngSpeedButton;
    lblCod: TLabel;
    edtCodigo: TDBEdit;
    dsEstoque: TDataSource;
    lblDesc: TLabel;
    edtDesc: TDBEdit;
    lblPreco: TLabel;
    edtTamanho: TDBEdit;
    lblQnt: TLabel;
    BarraStatus: TStatusBar;
    edtQnt: TJvSpinEdit;
    edtReferencia: TDBEdit;
    Label1: TLabel;
    edtPreco: TDBEdit;
    Label2: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
  private
    ItemTotal:integer;
    procedure Adicionar;
    procedure Proximo;
    procedure Anterior;
    procedure AtualizarCaption;
    procedure AtualizarStatus;
    procedure AtualizarBotoes;
    procedure AtualizarQuantidade;
  public
    procedure resizeForm(numPanels:integer);
  end;

var
  FrmAddAllEtiqueta: TFrmAddAllEtiqueta;

implementation

uses UDtmPrincipal, UFrmPrincipal;

{$R *.dfm}

procedure TFrmAddAllEtiqueta.Adicionar;
begin
  if((edtQnt.Text <> '')AND(strToInt(edtQnt.Text) > 0)) then
    begin

      FrmPrincipal.AddEtiqueta(StrToInt(edtQnt.Text));
      Proximo;
    end
  else
    MessageDlg('Erro! � necess�rio inserir uma quantidade v�lida para adicionar etiquetas!', mtWarning, [mbOK], 0);
end;

procedure TFrmAddAllEtiqueta.Anterior;
begin
  if not(DtmPrincipal.cdsEstoque.RecNo = 1) then
    begin
      DtmPrincipal.cdsEstoque.Prior;
      AtualizarBotoes;
      AtualizarCaption;
      AtualizarStatus;
      AtualizarQuantidade;
    end;
end;

procedure TFrmAddAllEtiqueta.AtualizarBotoes;
begin
  if FrmPrincipal.PesqItemAddEtiqueta(DtmPrincipal.cdsEstoqueCodigo.AsInteger) then
    btnAdicionar.Enabled := false
  else
    btnAdicionar.Enabled := true;

  if (DtmPrincipal.cdsEstoque.RecNo > 1) then
    btnAnterior.Enabled := true
  else
    btnAnterior.Enabled := false;

  if (DtmPrincipal.cdsEstoque.RecNo < ItemTotal) then
    btnProximo.Enabled := true
  else
    btnProximo.Enabled := false;
end;

procedure TFrmAddAllEtiqueta.AtualizarCaption;
begin
  FrmAddAllEtiqueta.Caption := '  ' + DtmPrincipal.cdsEstoqueDescricao.AsString;
end;

procedure TFrmAddAllEtiqueta.AtualizarQuantidade;
begin
  edtQnt.Value := DtmPrincipal.cdsEstoqueQuantidade.AsExtended;
end;

procedure TFrmAddAllEtiqueta.AtualizarStatus;
begin
  BarraStatus.Panels[0].Text := 'Total de itens: ' + IntToStr(ItemTotal);
  BarraStatus.Panels[1].Text := 'Item atual: ' + IntToStr(DtmPrincipal.cdsEstoque.RecNo);
  BarraStatus.Panels[2].Text := 'Total de etiquetas: ' + IntToStr(DtmPrincipal.cdsEtiqueta.RecordCount);
end;

procedure TFrmAddAllEtiqueta.btnAdicionarClick(Sender: TObject);
begin
  Adicionar;
end;

procedure TFrmAddAllEtiqueta.btnAnteriorClick(Sender: TObject);
begin
  Anterior;
end;

procedure TFrmAddAllEtiqueta.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmAddAllEtiqueta.btnProximoClick(Sender: TObject);
begin
  Proximo;
end;

procedure TFrmAddAllEtiqueta.FormShow(Sender: TObject);
begin
  resizeForm(3);

  ItemTotal := DtmPrincipal.cdsEstoque.RecordCount;


  AtualizarBotoes;
  AtualizarCaption;
  AtualizarStatus;
  AtualizarQuantidade;
end;

procedure TFrmAddAllEtiqueta.Proximo;
begin
  if not DtmPrincipal.cdsEstoque.Eof then
    begin
      DtmPrincipal.cdsEstoque.Next;
      AtualizarBotoes;
      AtualizarCaption;
      AtualizarStatus;
      AtualizarQuantidade;
    end;
end;

procedure TFrmAddAllEtiqueta.resizeForm(numPanels:integer);
var
  I:integer;
begin
  for I := 0 to numPanels - 1 do
    BarraStatus.Panels[I].Width := Trunc(FrmAddAllEtiqueta.Width / numPanels);
end;

end.
