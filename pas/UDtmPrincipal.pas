unit UDtmPrincipal;

interface

uses
  SysUtils, Classes, WideStrings, DBXFirebird, DB, SqlExpr, FMTBcd, DBClient,
  Provider, RpCon, RpConDS, RpRave, RpDefine, RpBase, RpSystem, RvLDCompiler;

type
  TDtmPrincipal = class(TDataModule)
    ConDb: TSQLConnection;
    sdsEstoque: TSQLDataSet;
    dspEstoque: TDataSetProvider;
    cdsEstoque: TClientDataSet;
    cdsEtiqueta: TClientDataSet;
    RvSystem: TRvSystem;
    rvpEtiqueta: TRvProject;
    rvDsEtiqueta: TRvDataSetConnection;
    cdsEtiquetaId: TIntegerField;
    cdsEtiquetaCodigo: TStringField;
    sdsEstoqueCodigo: TIntegerField;
    sdsEstoqueDescricao: TStringField;
    sdsEstoqueQuantidade: TFMTBCDField;
    sdsEstoquePreco: TFMTBCDField;
    sdsEstoqueReferencia: TStringField;
    sdsEstoqueTamanho: TStringField;
    cdsEstoqueCodigo: TIntegerField;
    cdsEstoqueDescricao: TStringField;
    cdsEstoqueQuantidade: TFMTBCDField;
    cdsEstoquePreco: TFMTBCDField;
    cdsEstoqueReferencia: TStringField;
    cdsEstoqueTamanho: TStringField;
    cdsEtiquetaDescricao: TStringField;
    cdsEtiquetaTamanho: TIntegerField;
    cdsEtiquetaPreco: TCurrencyField;
    cdsEtiquetaReferencia: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DtmPrincipal: TDtmPrincipal;

implementation

{$R *.dfm}

end.
