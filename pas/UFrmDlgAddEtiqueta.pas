unit UFrmDlgAddEtiqueta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvSpin, Buttons, PngSpeedButton, ExtCtrls;

type
  TFrmDlgAddEtiqueta = class(TForm)
    pTop: TPanel;
    pBottom: TPanel;
    btnAdicionar: TPngSpeedButton;
    btnCancelar: TPngSpeedButton;
    lblAddEtq: TLabel;
    edtQntEtq: TJvSpinEdit;
    lblEtq: TLabel;
    procedure btnAdicionarClick(Sender: TObject);
    procedure edtQntEtqKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDlgAddEtiqueta: TFrmDlgAddEtiqueta;

implementation

uses UFrmPrincipal, UDtmPrincipal;

{$R *.dfm}

procedure TFrmDlgAddEtiqueta.btnAdicionarClick(Sender: TObject);
begin
if (edtQntEtq.Text <> '') AND (StrToInt(edtQntEtq.Text) > 0) then
    begin
        FrmPrincipal.AddEtiqueta(StrToInt(edtQntEtq.Text));
        Close;
    end
  else
    MessageDlg('Erro! � necess�rio inserir uma quantidade v�lida para adicionar etiquetas!', mtWarning, [mbOK], 0);
end;

procedure TFrmDlgAddEtiqueta.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmDlgAddEtiqueta.edtQntEtqKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnAdicionar.Click;
end;

procedure TFrmDlgAddEtiqueta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Close;
end;

procedure TFrmDlgAddEtiqueta.FormShow(Sender: TObject);
begin
  edtQntEtq.Text := DtmPrincipal.cdsEstoqueQuantidade.AsString;
  edtQntEtq.SetFocus;
end;

end.
