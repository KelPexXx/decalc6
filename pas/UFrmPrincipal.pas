unit UFrmPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, StdCtrls, Mask, JvExMask, JvToolEdit,
  JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, Menus,
  Spin, DB, ComCtrls, JvExComCtrls, JvDateTimePicker, Registry, Buttons,
  PngSpeedButton, RvLDCompiler, IniFiles;

type
  TFrmPrincipal = class(TForm)
    pBaixo: TPanel;
    dgPrincipal: TDBGrid;
    mMenuPrincipal: TMainMenu;
    menuOpcoes: TMenuItem;
    menuSobre: TMenuItem;
    dsEstoque: TDataSource;
    gbOpcoesPadroes: TGroupBox;
    cbAddQtdEstq: TCheckBox;
    btnImprimirEtiqueta: TPngSpeedButton;
    btnExecSequencia: TPngSpeedButton;
    Panel1: TPanel;
    gbPesquisa: TGroupBox;
    btnPesquisar: TPngSpeedButton;
    Panel4: TPanel;
    edtPesquisa: TEdit;
    rgPesquisa: TRadioGroup;
    gbEtiqueta: TGroupBox;
    lblEtiquetaQnt: TLabel;
    lblEtiquetaDesc: TLabel;
    pEtiquetaBaixo: TPanel;
    btnEtiquetaAdd: TPngSpeedButton;
    btnEtiquetaLista: TPngSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure menuOpcoesClick(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnExecSequenciaClick(Sender: TObject);
    procedure btnEtiquetaAddClick(Sender: TObject);
    procedure btnEtqPrecoAddAllClick(Sender: TObject);
    procedure btnEtiquetaListaClick(Sender: TObject);
    procedure dgPrincipalDblClick(Sender: TObject);
    procedure edtPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure menuSobreClick(Sender: TObject);
    procedure btnImprimirEtiquetaClick(Sender: TObject);
    procedure cbAddQtdEstqExit(Sender: TObject);
    procedure pRightPesquisaResize(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  private
    sSelect, sWhere, sCodigo, sDesc, sReferencia: String;
    procedure Pesquisar;
    procedure PreSelect;
    procedure InitDataSets;
  public
    rBase, rUser, rSenha: String;
    rNomTabela, rCodigo, rDesc, rPreco, rQntAtual, rReferencia, rBarras,
      rTamanho: String;
    rAddQtdEstoque: Boolean;
    IdEtqPreco: integer;
    procedure CarregarConfiguracoes;
    procedure SalvarConfiguracoes;
    procedure AtualizaContadores;
    procedure PostAddEtiqueta;
    procedure PostRemEtiqueta;
    procedure PostRemEtiquetaCod(codigo: integer);
    procedure PostRemEtiquetaAll;
    procedure AddEtiqueta(count: integer);
    function PesqItemAddEtiqueta(codigo: integer): Boolean;
    function ValidaPesquisaEtiqueta: Boolean;
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

uses UDtmPrincipal, UFrmOpcoes, UFrmAddAllEtiqueta,
  UFrmDlgAddEtiqueta, UFrmLstEtiqueta, UFrmSobrePrograma;

{$R *.dfm}

procedure TFrmPrincipal.cbAddQtdEstqExit(Sender: TObject);
begin
  rAddQtdEstoque := cbAddQtdEstq.Checked;
end;

procedure TFrmPrincipal.btnPesquisaClick(Sender: TObject);
begin
  Pesquisar;
end;

procedure TFrmPrincipal.Button1Click(Sender: TObject);
begin

end;

// PROCEDIMENTOS DE EXECU��O

function TFrmPrincipal.PesqItemAddEtiqueta(codigo: integer): Boolean;
begin
  result := False;
  if DtmPrincipal.cdsEtiqueta.Active then
  begin
    DtmPrincipal.cdsEtiqueta.First;
    while not(DtmPrincipal.cdsEtiqueta.Eof) do
    begin
      if DtmPrincipal.cdsEtiquetaCodigo.AsInteger = codigo then
      begin
        result := True;
        break;
      end;
      DtmPrincipal.cdsEtiqueta.Next;
    end;
  end;
end;

procedure TFrmPrincipal.AddEtiqueta(count: integer);
var
  I, iResposta: integer;
begin
  if DtmPrincipal.cdsEstoque.Active then
  begin
    if PesqItemAddEtiqueta(DtmPrincipal.cdsEstoqueCodigo.AsInteger) then
    begin
      if MessageDlg
        ('Esta etiqueta de pre�o j� se encontra na lista, deseja adicionar novamente?',
        mtInformation, mbYesNo, 0) = mrYes then
      begin
        for I := 1 to count do
          PostAddEtiqueta;
      end;
    end
    else
    begin
      if ((DtmPrincipal.cdsEstoqueTamanho.AsString = EmptyStr) or
        (DtmPrincipal.cdsEstoqueReferencia.AsString = EmptyStr)) then
      begin
        iResposta := MessageDlg('Aten��o!' + #13 + 'O item n�o possui todas as '
          + #13 + 'informa��es necess�rias para gerar a etiqueta.' + #13 +
          'Deseja adicionar sem estas informa��es? ' + #13 +
          ' (Ser� utilizado 0 (zero) como valor padr�o)', mtWarning,
          [mbYes, mbNo], 0, mbYes);
        if iResposta = mrNo then
          Exit;
      end;
      for I := 1 to count do
        PostAddEtiqueta;
    end;
    AtualizaContadores;
  end;
end;

procedure TFrmPrincipal.AtualizaContadores;
begin
  lblEtiquetaQnt.Caption := IntToStr(DtmPrincipal.cdsEtiqueta.RecordCount);
end;

procedure TFrmPrincipal.Pesquisar;
var
  count: integer;
begin
  count := 0;
  DtmPrincipal.cdsEstoque.Close;
  DtmPrincipal.sdsEstoque.CommandText := sSelect + ' ' + sWhere;
  case rgPesquisa.ItemIndex of
    2:
      begin
        DtmPrincipal.sdsEstoque.CommandText :=
          DtmPrincipal.sdsEstoque.CommandText + ' ' + sCodigo;
        inc(count);
      end;
    1:
      begin
        DtmPrincipal.sdsEstoque.CommandText :=
          DtmPrincipal.sdsEstoque.CommandText + ' ' + sDesc;
        inc(count);
      end;
    0:
      begin
        DtmPrincipal.sdsEstoque.CommandText :=
          DtmPrincipal.sdsEstoque.CommandText + ' ' + sReferencia;
        inc(count);
      end;
  end;

  DtmPrincipal.cdsEstoque.FetchParams;

  case rgPesquisa.ItemIndex of
    2:
      begin
        DtmPrincipal.cdsEstoque.Params.ParamByName('sPesquisaCod').AsString :=
          edtPesquisa.Text;
      end;
    1:
      begin
        DtmPrincipal.cdsEstoque.Params.ParamByName('sPesquisaDesc').AsString :=
          '%' + edtPesquisa.Text + '%';
      end;
    0:
      begin
        DtmPrincipal.cdsEstoque.Params.ParamByName('sPesquisaReferencia')
          .AsString := '%' + edtPesquisa.Text + '%';
      end;
  end;

  DtmPrincipal.cdsEstoque.Open;
  dgPrincipal.SetFocus;
end;

procedure TFrmPrincipal.PostAddEtiqueta;
var
  cDesconto, cParcelas: Currency;
begin
  if not DtmPrincipal.cdsEtiqueta.Active then
    DtmPrincipal.cdsEtiqueta.Open;
  DtmPrincipal.cdsEtiqueta.Append;
  DtmPrincipal.cdsEtiquetaId.AsInteger := IdEtqPreco;
  inc(IdEtqPreco, 1);
  DtmPrincipal.cdsEtiquetaCodigo.AsString :=
    DtmPrincipal.cdsEstoqueCodigo.AsString;
  DtmPrincipal.cdsEtiquetaDescricao.AsString :=
    DtmPrincipal.cdsEstoqueDescricao.AsString;
  DtmPrincipal.cdsEtiquetaTamanho.AsInteger :=
    StrToIntDef(DtmPrincipal.cdsEstoqueTamanho.AsString, 0);
  DtmPrincipal.cdsEtiquetaPreco.AsCurrency :=
    DtmPrincipal.cdsEstoquePreco.AsCurrency;
  DtmPrincipal.cdsEtiquetaReferencia.AsInteger :=
    StrToIntDef(DtmPrincipal.cdsEstoqueReferencia.AsString, 0);
  DtmPrincipal.cdsEtiqueta.Post;
end;

procedure TFrmPrincipal.PostRemEtiqueta;
begin
  try
    if not DtmPrincipal.cdsEtiqueta.IsEmpty then
    begin
      DtmPrincipal.cdsEtiqueta.Delete;
    end;
    AtualizaContadores;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsEtiqueta.Cancel;
      DtmPrincipal.cdsEtiqueta.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmPrincipal.PostRemEtiquetaAll;
begin
  DtmPrincipal.cdsEtiqueta.First;
  while not(DtmPrincipal.cdsEtiqueta.Eof) do
  begin
    PostRemEtiqueta;
  end;
end;

procedure TFrmPrincipal.PostRemEtiquetaCod(codigo: integer);
begin
  DtmPrincipal.cdsEtiqueta.Close;
  DtmPrincipal.cdsEtiqueta.Filter := 'Codigo = ' + IntToStr(codigo);
  DtmPrincipal.cdsEtiqueta.Filtered := True;
  DtmPrincipal.cdsEtiqueta.Open;
  DtmPrincipal.cdsEtiqueta.First;
  while not(DtmPrincipal.cdsEtiqueta.Eof) do
  begin
    PostRemEtiqueta;
  end;
  DtmPrincipal.cdsEtiqueta.Close;
  DtmPrincipal.cdsEtiqueta.Filter := '';
  DtmPrincipal.cdsEtiqueta.Filtered := False;;
  DtmPrincipal.cdsEtiqueta.Open;
end;

procedure TFrmPrincipal.PreSelect;
begin
  // Preparando Select Personalizado
  sSelect := ' SELECT                             ' + '   ' + rCodigo +
    ' as "Codigo",         ' + '   ' + rDesc + ' as "Descricao",        ' +
    '   ' + rQntAtual + ' as "Quantidade",   ' + '   ' + rPreco +
    ' as "Preco",           ' + '   ' + rReferencia + ' as "Referencia", ' +
    '   ' + rTamanho + ' as "Tamanho",       ' +
    '   COD_BARRA as "C�d Barras"        ' +
    ' FROM                               ' +
    '   V_ESTOQUE_BROWSE                 ';

  sWhere := 'WHERE';
  sCodigo := rCodigo + ' LIKE :sPesquisaCod';
  sDesc := 'UPPER(' + rDesc + ') LIKE :sPesquisaDesc';
  sReferencia := rReferencia + ' LIKE :sPesquisaReferencia';

end;

procedure TFrmPrincipal.pRightPesquisaResize(Sender: TObject);
begin

end;

// FIM PROCEDIMENTOS DE EXECU��O

// PR� DEFINI��ES

procedure TFrmPrincipal.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  edtPesquisa.SetFocus;
  if MessageDlg('Deseja fechar o Decalc6?', mtInformation, mbYesNo, 0) = mrYes
  then
  begin
    SalvarConfiguracoes;
    Application.Terminate;
  end
  else
    CanClose := False;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
  DtmPrincipal := TDtmPrincipal.Create(Self);

  CarregarConfiguracoes;

  // Setando Variaveis
  IdEtqPreco := 1;

  DtmPrincipal.ConDb.Close;

  DtmPrincipal.ConDb.Params.Values['Database'] := rBase;
  DtmPrincipal.ConDb.Params.Values['User_Name'] := rUser;
  DtmPrincipal.ConDb.Params.Values['Password'] := rSenha;

  try
    DtmPrincipal.ConDb.Open;
  except
    on E: Exception do
      MessageDlg('Erro ao conectar, ' + E.Message, mtError, [mbOK], 0);
  end;

end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
  lblEtiquetaQnt.Caption := '0';

  cbAddQtdEstq.Checked := rAddQtdEstoque;

  PreSelect;
  InitDataSets;

  edtPesquisa.SetFocus;
end;

procedure TFrmPrincipal.InitDataSets;
begin
  // Criando DataSet Vaziu para cadastrar as etiquetas
  try
    DtmPrincipal.cdsEtiqueta.CreateDataSet;
    DtmPrincipal.cdsEtiqueta.EmptyDataSet;
    DtmPrincipal.cdsEtiqueta.Open;
  except
    on E: Exception do
      MessageDlg('Erro ao criar tempor�rio', mtError, [mbOK], 0);
  end;
end;

// FIM PR� DEFINIC�ES

// JANELAS

procedure TFrmPrincipal.menuOpcoesClick(Sender: TObject);
begin
  if not(Assigned(FrmOpcoes)) then
    FrmOpcoes := TFrmOpcoes.Create(Self);
  FrmOpcoes.Show;
end;

procedure TFrmPrincipal.menuSobreClick(Sender: TObject);
begin
  if not Assigned(FrmSobrePrograma) then
    FrmSobrePrograma := TFrmSobrePrograma.Create(Self);
  FrmSobrePrograma.Show;
end;

procedure TFrmPrincipal.btnEtqPrecoAddAllClick(Sender: TObject);
begin
  if not Assigned(FrmAddAllEtiqueta) then
    Application.CreateForm(TFrmAddAllEtiqueta, FrmAddAllEtiqueta);
  FrmAddAllEtiqueta.ShowModal;
end;

procedure TFrmPrincipal.btnEtiquetaAddClick(Sender: TObject);
begin
  if ValidaPesquisaEtiqueta then
    if cbAddQtdEstq.Checked then
      AddEtiqueta(DtmPrincipal.cdsEstoqueQuantidade.AsInteger)
    else
    begin
      if not Assigned(FrmDlgAddEtiqueta) then
        FrmDlgAddEtiqueta := TFrmDlgAddEtiqueta.Create(Self);
      FrmDlgAddEtiqueta.ShowModal;
    end;
end;

procedure TFrmPrincipal.btnEtiquetaListaClick(Sender: TObject);
begin
  if not Assigned(FrmLstEtiqueta) then
    FrmLstEtiqueta := TFrmLstEtiqueta.Create(Self);
  FrmLstEtiqueta.ShowModal;

end;

procedure TFrmPrincipal.btnExecSequenciaClick(Sender: TObject);
begin
  if ValidaPesquisaEtiqueta then
  begin
    if not Assigned(FrmAddAllEtiqueta) then
      FrmAddAllEtiqueta := TFrmAddAllEtiqueta.Create(Self);
    FrmAddAllEtiqueta.ShowModal;
  end
  else
  begin
    MessageDlg('Erro: � necess�rio efetuar uma pesquisa primeiro!', mtError,
      [mbOK], 0);
  end;
end;

procedure TFrmPrincipal.btnImprimirEtiquetaClick(Sender: TObject);
begin
  if DtmPrincipal.cdsEtiqueta.RecordCount = 0 then
    MessageDlg('Erro: � necess�rio cadastrar etiquetas antes de imprimir!',
      mtError, [mbOK], 0)
  else
    DtmPrincipal.rvpEtiqueta.SetParam('logo',
      ExtractFilePath(Application.ExeName) + 'logo.bmp');
  DtmPrincipal.rvpEtiqueta.Execute;
end;

// FIM JANELAS

// CONFIGURA��ES E REGISTRO

procedure TFrmPrincipal.SalvarConfiguracoes;
var
  padrao: TRegistry;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\KelPexXx\Decalc6', True);

  padrao.WriteBool('rAddQtdEstoque', rAddQtdEstoque);
  padrao.WriteString('rBase', rBase);
  padrao.WriteString('rUser', rUser);
  padrao.WriteString('rSenha', rSenha);
  padrao.WriteString('rNomTabela', rNomTabela);
  padrao.WriteString('rCodigo', rCodigo);
  padrao.WriteString('rDesc', rDesc);
  padrao.WriteString('rPreco', rPreco);
  padrao.WriteString('rQntAtual', rQntAtual);
  padrao.WriteString('rReferencia', rReferencia);
  padrao.WriteString('rTamanho', rTamanho);
  padrao.CloseKey();
  padrao.Free;
end;

function TFrmPrincipal.ValidaPesquisaEtiqueta: Boolean;
var
  iErros: integer;
begin
  iErros := 0;
  if DtmPrincipal.cdsEstoque.Active then
  begin
    if DtmPrincipal.cdsEstoque.RecordCount <= 1 then
    begin
      if (DtmPrincipal.cdsEstoqueCodigo.AsString = EmptyStr) then
        inc(iErros, 1);
      if (DtmPrincipal.cdsEstoqueDescricao.AsString = EmptyStr) then
        inc(iErros, 1);
      if (DtmPrincipal.cdsEstoqueQuantidade.AsString = EmptyStr) then
        inc(iErros, 1);
      if (DtmPrincipal.cdsEstoquePreco.AsString = EmptyStr) then
        inc(iErros, 1);
      if (DtmPrincipal.cdsEstoqueReferencia.AsString = EmptyStr) then
        inc(iErros, 1);
      if (DtmPrincipal.cdsEstoqueTamanho.AsString = EmptyStr) then
        inc(iErros, 1);
    end;
  end
  else
    inc(iErros, 1);

  result := iErros = 0;
end;

procedure TFrmPrincipal.CarregarConfiguracoes;
var
  padrao: TRegistry;
  oArquivo: TIniFile;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\KelPexXx\Decalc6', True);

  oArquivo := TIniFile.Create(ChangeFileExt(Application.ExeName,'ini'));
  try
  // rAddQtdEstoque
  if padrao.ValueExists('rAddQtdEstoque') then
    rAddQtdEstoque := padrao.ReadBool('rAddQtdEstoque')
  else
  begin
    padrao.WriteBool('rAddQtdEstoque', False);
    rAddQtdEstoque := padrao.ReadBool('rAddQtdEstoque');
  end;
  // rBase
  if padrao.ValueExists('rBase') then
    rBase := padrao.ReadString('rBase')
  else
  begin
    padrao.WriteString('rBase', 'C:\CLIPP.FDB');
    rBase := padrao.ReadString('rBase');
  end;
  // User
  if padrao.ValueExists('rUser') then
    rUser := padrao.ReadString('rUser')
  else
  begin
    padrao.WriteString('rUser', 'sysdba');
    rUser := padrao.ReadString('rUser');
  end;
  // Senha
  if padrao.ValueExists('Srenha') then
    rSenha := padrao.ReadString('rSenha')
  else
  begin
    padrao.WriteString('rSenha', 'masterkey');
    rSenha := padrao.ReadString('rSenha');
  end;
  // Tabela
  if padrao.ValueExists('rNomTabela') then
    rNomTabela := padrao.ReadString('rNomTabela')
  else
  begin
    padrao.WriteString('rNomTabela', 'V_ESTOQUE_BROWSE');
    rNomTabela := padrao.ReadString('rNomTabela');
  end;
  // Codigo
  if padrao.ValueExists('rCodigo') then
    rCodigo := padrao.ReadString('rCodigo')
  else
  begin
    padrao.WriteString('rCodigo', 'ID_ESTOQUE');
    rCodigo := padrao.ReadString('rCodigo');
  end;
  // Descricao
  if padrao.ValueExists('rDesc') then
    rDesc := padrao.ReadString('rDesc')
  else
  begin
    padrao.WriteString('rDesc', 'DESCRICAO');
    rDesc := padrao.ReadString('rDesc');
  end;
  // Preco
  if padrao.ValueExists('rPreco') then
    rPreco := padrao.ReadString('rPreco')
  else
  begin
    padrao.WriteString('rPreco', 'PRC_VENDA');
    rPreco := padrao.ReadString('rPreco');
  end;
  // Qtd Atual
  if padrao.ValueExists('rQntAtual') then
    rQntAtual := padrao.ReadString('rQntAtual')
  else
  begin
    padrao.WriteString('rQntAtual', 'QTD_ATUAL');
    rQntAtual := padrao.ReadString('rQntAtual');
  end;
  // Referencia
  if padrao.ValueExists('rReferencia') then
    rReferencia := padrao.ReadString('rReferencia')
  else
  begin
    padrao.WriteString('rReferencia', 'REFERENCIA');
    rReferencia := padrao.ReadString('rReferencia');
  end;
  // Tamanho
  if padrao.ValueExists('rTamanho') then
    rTamanho := padrao.ReadString('rTamanho')
  else
  begin
    padrao.WriteString('rTamanho', 'ADICIONAL1');
    rTamanho := padrao.ReadString('rTamanho');
  end;
  padrao.CloseKey();
  padrao.Free;
  finally
    oArquivo.Free;
  end;
end;

procedure TFrmPrincipal.dgPrincipalDblClick(Sender: TObject);
begin
  btnEtiquetaAdd.Click;
end;

procedure TFrmPrincipal.edtPesquisaKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnPesquisar.Click;
end;

// FIM CONFIGURA��ES E REGISTRO

end.
