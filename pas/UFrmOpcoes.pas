unit UFrmOpcoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, PngSpeedButton;

type
  TFrmOpcoes = class(TForm)
    gbConexao: TGroupBox;
    pBotoes: TPanel;
    edtBanco: TLabeledEdit;
    edtUser: TLabeledEdit;
    edtSenha: TLabeledEdit;
    dBuscaBanco: TOpenDialog;
    btnCancelar: TPngSpeedButton;
    btnAdicionar: TPngSpeedButton;
    btnBuscar: TPngSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmOpcoes: TFrmOpcoes;

implementation

uses UDtmPrincipal, UFrmPrincipal;

{$R *.dfm}

procedure TFrmOpcoes.btnAdicionarClick(Sender: TObject);
begin
    if MessageDlg('� necess�rio fechar e abrir o programa para aplicar as configura��es?', mtInformation, mbOKCancel, 0)= mrOk  then
      begin
        FrmPrincipal.rBase := edtBanco.Text;
        FrmPrincipal.rUser := edtUser.Text;
        FrmPrincipal.rSenha := edtSenha.Text;
        DtmPrincipal.cdsEstoque.Close;
        DtmPrincipal.sdsEstoque.Close;
        DtmPrincipal.ConDb.Close;
        DtmPrincipal.ConDb.Params.Values['Database'] := FrmPrincipal.rBase;
        DtmPrincipal.ConDb.Params.Values['User_Name'] := FrmPrincipal.rUser;
        DtmPrincipal.ConDb.Params.Values['Password'] := FrmPrincipal.rSenha;
        try
          try
           DtmPrincipal.ConDb.Open;
          except
            on E: Exception do
             MessageDlg('Erro ao conectar, '+ E.Message, mtError, [mbOK], 0);
          end;
        finally
          DtmPrincipal.sdsEstoque.Open;
          DtmPrincipal.cdsEstoque.Open;
          FrmPrincipal.SalvarConfiguracoes;
          Application.Terminate;
        end;
      end
    else
      close;

end;

procedure TFrmOpcoes.btnBuscarClick(Sender: TObject);
begin
  if dBuscaBanco.Execute then
    edtBanco.Text := dBuscaBanco.FileName;
end;

procedure TFrmOpcoes.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmOpcoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmOpcoes);
end;

procedure TFrmOpcoes.FormShow(Sender: TObject);
begin
  edtBanco.Text := FrmPrincipal.rBase;
  dBuscaBanco.FileName := FrmPrincipal.rBase;
  edtUser.Text := FrmPrincipal.rUser;
  edtSenha.Text := FrmPrincipal.rSenha;
end;

end.
