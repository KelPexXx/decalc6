unit UFrmLstEtiqueta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ExtCtrls, StdCtrls, Vcl.Buttons, PngSpeedButton;

type
  TFrmLstEtiqueta = class(TForm)
    pBaixo: TPanel;
    pLateral: TPanel;
    dsEtiqueta: TDataSource;
    dgEtqPreco: TDBGrid;
    btnRemoverEtqCod: TPngSpeedButton;
    btnRemoverEtq: TPngSpeedButton;
    btnRemoverEtqAll: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    btnImpEtq: TPngSpeedButton;
    procedure btnFecharClick(Sender: TObject);
    procedure btnRemoverEtqClick(Sender: TObject);
    procedure btnRemoverEtqCodClick(Sender: TObject);
    procedure btnRemoverEtqAllClick(Sender: TObject);
    procedure btnImpEtqClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmLstEtiqueta: TFrmLstEtiqueta;

implementation

uses UDtmPrincipal, UFrmPrincipal;

{$R *.dfm}

procedure TFrmLstEtiqueta.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmLstEtiqueta.btnImpEtqClick(Sender: TObject);
begin
  FrmPrincipal.btnImprimirEtiqueta.Click;
end;

procedure TFrmLstEtiqueta.btnRemoverEtqAllClick(Sender: TObject);
begin
  FrmPrincipal.PostRemEtiquetaAll;
end;

procedure TFrmLstEtiqueta.btnRemoverEtqClick(Sender: TObject);
begin
  FrmPrincipal.PostRemEtiqueta;
end;

procedure TFrmLstEtiqueta.btnRemoverEtqCodClick(Sender: TObject);
begin
  FrmPrincipal.PostRemEtiquetaCod(DtmPrincipal.cdsEtiquetaCodigo.AsInteger);
end;

end.
