program Decalc6;

uses
  Forms,
  UDtmPrincipal in '..\pas\UDtmPrincipal.pas' {DtmPrincipal: TDataModule},
  UFrmPrincipal in '..\pas\UFrmPrincipal.pas' {FrmPrincipal},
  UFrmOpcoes in '..\pas\UFrmOpcoes.pas' {FrmOpcoes},
  UFrmDlgAddEtiqueta in '..\pas\UFrmDlgAddEtiqueta.pas' {FrmDlgAddEtiqueta},
  UFrmAddAllEtiqueta in '..\pas\UFrmAddAllEtiqueta.pas' {FrmAddAllEtiqueta},
  UFrmLstEtiqueta in '..\pas\UFrmLstEtiqueta.pas' {FrmLstEtiqueta},
  UFrmSobrePrograma in '..\pas\UFrmSobrePrograma.pas' {FrmSobrePrograma},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Karabast TM');
  Application.Title := 'Decalc6 ';
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.Run;
end.
